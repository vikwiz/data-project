import os, pandas as pd
from datetime import datetime
#from ggplot import *
from math import *
import numpy, random, time
from sklearn.neighbors import KNeighborsClassifier

dfSpray = pd.DataFrame.from_csv('spray.csv')
dfTrain = pd.DataFrame.from_csv('train.csv')
dfWeather = pd.DataFrame.from_csv('weather.csv')
dfTest = pd.DataFrame.from_csv('test.csv')


dfDistTemp = pd.DataFrame({'Id':range(1,len(dfTrain)+1),'Distance':[0.0]*len(dfTrain),'Latitude':dfTrain.Latitude,'Longitude':dfTrain.Longitude, 'AddressAccuracy':dfTrain.AddressAccuracy})
dfDistTemp.index = dfDistTemp.Id


#Test has 8 species of Culex incl "Unspecified Culex" whereas Train only has 7 species
spcs = pd.unique(dfTest.Species)
#Test has 151 unique addresses whereas Train only has 138 unique addresses
addr = pd.unique(dfTest.Address)
species = {}
address = {}
spciesWN = []
maxDateRange = 0
K = 3
neigh = KNeighborsClassifier(n_neighbors=K)
clf = None

#p = ggplot(aes(x='Latitude',y='Longitude'), data = dfSpray)+geom_point()
#p.draw()

def createSubsetTest(setlen, testortrain):
	global dfTest, dfTrain
	if testortrain == "test":
		dfTst = dfTest.copy(deep=False)
	else:
		dfTst = dfTrain.copy(deep=False)
	dfTest = dfTst[dfTst.Date>datetime.strptime('2015-01-01','%Y-%m-%d')]
	maxlen = len(dfTst) - 1
	while setlen > 0:
		a = dfTst.iloc[random.randint(0,maxlen)]
		if not (a.ID in dfTest.ID):
			dfTest = dfTest.append(a)
			setlen = setlen - 1
			#if testortrain == "train":
			#   dfTrain = dfTrain[dfTrain.ID != a.ID]
	return [dfTest, dfTrain]

def normalize():
	global dfTrain, dfTest, dfTst, species, address, speciesWN, maxDateRange
	for i in range(0,len(spcs)):
		species[spcs[i]] = i
	for i in range(0,len(addr)):
		address[addr[i]] = i
	#Normalize Training Data
	dfTrain.Species = dfTrain.Species.apply(lambda x: species[x])
	dfTrain.Address = dfTrain.Address.apply(lambda x: address[x])
	speciesWN = pd.unique(dfTrain.Species[dfTrain.WnvPresent == 1])
	dfTrain.Date = dfTrain.Date.apply(lambda x:datetime.strptime(x,"%Y-%m-%d"))
	dfTrain['ID'] = pd.Series(range(1,len(dfTrain)+1),index = dfTrain.index)
	#Normalize Testing Data
	dfTest.Species = dfTest.Species.apply(lambda x: species[x])
	dfTest.Address = dfTest.Address.apply(lambda x: address[x])
	dfTest.Date = dfTest.Date.apply(lambda x:datetime.strptime(x,"%Y-%m-%d"))
	dfTest['ID'] = pd.Series(range(1,len(dfTest)+1),index = dfTest.index)
	maxDateRange = (dfTrain.Date.max(axis=1)-dfTrain.Date.min(axis=1)).days


def getDistance(x,inst):
	a = sqrt(pow(float(x[3]) - float(inst.Latitude),2)+pow(float(x[4]) - float(inst.Longitude),2))
	return ((100.0-float(x[0])*float(inst.AddressAccuracy))*a) + x[1]


def getNeighbours(inst):
	global dfDistTemp
	#calculate difference in days
	dfDistTemp.Distance = dfTrain.Date.apply(lambda x:float(abs((inst.Date - x).days)))
	#calculate separation b/w coordinates weighted by accuracy
	dfDistTemp.Distance = dfDistTemp.apply(lambda x:getDistance(x,inst),axis=1)
	dfDistTemp = dfDistTemp.sort('Distance',ascending = 1)
	return dfDistTemp.Id.head(K)

def getResponse(kNeighbours):
	wnvList = dfTrain.loc[dfTrain['ID'].isin(kNeighbours)]['WnvPresent']
	resp = sum(list(wnvList))
	return int(resp%2)

def computeKNN(inst):
	return getResponse(getNeighbours(inst))

def getAccuracy(testSet, predictions):
	correct = 0
	for i in range(0,len(testSet)):
		if testSet.iloc[i].WnvPresent == list(predictions[predictions.ID == testSet.iloc[i].Id].WnvPresent)[0]:
			correct += 1
	return (correct/float(len(testSet))) * 100.0

def knn_sklearn(inst):
    global clf
    return list(clf.predict(inst))[0]

def run():
    global dfTest, dfTrain
    normalize()
	#dfTr = dfTrain.copy(deep=False)
    [dfTest, dfTrain] = createSubsetTest(500,"train")
    print len(dfTest), len(dfTrain)
    print "Normalized all DataFrames."
    tr = dfTrain[['Address','Species', 'Block','Latitude', 'Longitude','ID']]
    tst = dfTest[['Address','Species', 'Block','Latitude', 'Longitude','ID']]
    clf = neigh.fit(tr,dfTrain.WnvPresent)
    print clf
    print "Computing Output........"
    dfOut = pd.DataFrame({'Id':dfTest.ID,'WnvPresent':[0]*len(dfTest)})
	#dfOut.index = dfOut.ID
    start = time.time()
	#dfOut.WnvPresent = dfTest.apply(lambda x: computeKNN(x), axis = 1)
    for i in range(0,len(tst)):
        dfOut.iloc[i].WnvPresent = list(clf.predict(tst.iloc[i]))[0]
    end = time.time()
    print "Program took :", str(end - start)
    print len(dfOut)
    print "Accuracy is :", getAccuracy(dfOut,dfTest)
    dfOut = dfOut.drop('Id',1)
    dfOut.to_csv('output_knn.csv')
    print "Output written out. Done !"
    return [dfOut,tr,tst,clf, dfTest, dfTrain]



if __name__ == "__main__":
	[dfOut,tr,tst,clf, dfTest, dfTrain] = run()



